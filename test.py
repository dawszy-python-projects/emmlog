import requests
import os

def chckIfNvrFileExists(url, fname):
    import os
    import time
    fnamemtime = 0
    if os.path.isfile(fname):
        fnamemtime = os.path.getmtime(fname)
    else:
        print("no local data available")
    r=requests.head(url, verify=False)
    if r.status_code and fnamemtime:
        if b4toepoch(r.headers['Last-Modified']) > fnamemtime:
            print("new data available")
        else:
            print("no new data available")
    else:
        print("not possible to check if new data is available")

def dwnldFile(url, fname):
    import requests
    req = requests.get(url, stream=True, verify=False)
    chunk_size = 1024
    dwnldSize = 0
    with open(fname, 'wb') as file:
        for chunk in req.iter_content(chunk_size):
            file.write(chunk)
            dwnldSize += len(chunk)
            print("Downloading",fname, dwnldSize, "bytes", end="\r")
        print("")

def b4toepoch(before_time):
    import time
    temp_time = time.strptime(before_time, "%a, %d %b %Y %H:%M:%S %Z")
    epoch_time = time.mktime(temp_time)
    return epoch_time

chckIfNvrFileExists('http://www.country-files.com/cty/cty.plist','cty.plist')
dwnldFile('http://www.country-files.com/cty/cty.plist','cty.plist')
chckIfNvrFileExists('http://www.supercheckpartial.com/MASTER.SCP','master.scp')
dwnldFile('http://www.supercheckpartial.com/MASTER.SCP','master.scp')
chckIfNvrFileExists('https://lotw.arrl.org/lotw-user-activity.csv','lotw-user-activity.csv')
dwnldFile('https://lotw.arrl.org/lotw-user-activity.csv','lotw-user-activity.csv')
