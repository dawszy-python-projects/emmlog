#!/usr/bin/env python3

from tkinter import *
from tkinter.ttk import *
import plistlib

root = Tk()
call = StringVar()

with open('cty.plist', 'rb') as fcty:
  cty = plistlib.load(fcty)

def ctyData(call):
  print(call)
  cty_found = False
  cty_data = None
  tmp_call = call
  cty_call_len = len(call)
  while not cty_found:
    tmp_call = call[:cty_call_len]
    cty_data = cty.get(tmp_call)
    if (cty_data == None) & (cty_call_len >= 2):
      cty_call_len = cty_call_len-1
    else:
      cty_found = True
    if cty_call_len == 1:
      break
  if cty_data == None:
    cty_data = {'Country': '', 'CQZone': '', 'ITUZone': ''}
  return cty_data

def key_control(event):
  if event.char == "-":
    cmd.delete(0,len(cmd.get()))

def key(event):
  if event.char == "-":
    cmd.delete(0,len(cmd.get()))
  country.config(text=ctyData(cmd.get().upper())["Country"])
  cqzone.config(text=ctyData(cmd.get().upper())["CQZone"])
  ituzone.config(text=ctyData(cmd.get().upper())["ITUZone"])


Label(root,text="Country: ").grid(row=0,column=0)
country = Label(root)
country.grid(row=0,column=1)
Label(root,text="CQ Zone: ").grid(row=1,column=0)
cqzone = Label(root)
cqzone.grid(row=1,column=1)
Label(root,text="ITU Zone: ").grid(row=2,column=0)
ituzone = Label(root)
ituzone.grid(row=2,column=1)

cmd = Entry(root,textvariable=call,width=80)
cmd.grid(row=3,columnspan=2)
cmd.bind("<KeyPress>", key_control)
cmd.bind("<KeyRelease>", key)
cmd.focus_set()

print(cmd)
root.mainloop()
