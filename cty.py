#!/usr/bin/env python3

import sys
import os
import logging

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(levelname)s - %(message)s')
logging.debug('This is a log message.')


def dwnldFile(url, fname):
    import requests
    req = requests.get(url, stream=True, verify=False)
    chunk_size = 1024
    dwnldSize = 0
    with open(fname, 'wb') as file:
        for chunk in req.iter_content(chunk_size):
            file.write(chunk)
            dwnldSize += len(chunk)
            print("Downloading",fname, dwnldSize, "bytes", end="\r")
        print("")

def loadscpData(filename):
    try:
        scp_file = list()
        scp_file = [line.rstrip('\n') for line in open(filename)]
        return(scp_file)
    except:
        logging.debug("Loading of scp data failed. Exitinig...")
        sys.exit(1)


def loadctyData(filename):
    try:
        import plistlib
        with open(filename, 'rb') as fcty:
            cty_file = plistlib.load(fcty)
        return(cty_file)
    except:
        logging.debug("Loading of cty data failed. Exiting...")
        sys.exit(1)


def scpData(scp_call):
    scp_found = False
    if scp_call in scp:
        scp_found = True
    return(scp_found)
    
# dopisać funkcje, ze uzyciem modułu "re" aby podpowiadać prawidłowe znaki


def ctyData(cty_call):
    cty_found = False
    cty_data = None
    tmp_call = cty_call
    cty_call_len = len(cty_call)
    orig_cty_call_len = cty_call_len
    if cty_call_len >= 1:
        while not cty_found:
            tmp_call = cty_call[:cty_call_len]
            cty_data = cty.get(tmp_call)
            if cty_data == None or (cty_data["ExactCallsign"] == True and orig_cty_call_len != cty_call_len):
                cty_call_len = cty_call_len-1
            else:
                cty_found = True
                break
            if cty_call_len == 0:
                break

    if not cty_found:
        return None
    else:
        return cty_data


print("Downloading CTY file")
dwnldFile('http://www.country-files.com/cty/cty.plist', 'cty.plist')
print("Download completed")
print("Downloading SCP file")
dwnldFile('http://www.supercheckpartial.com/MASTER.SCP', 'master.scp')
print("Download completed")

cty = loadctyData("cty.plist")
scp = loadscpData("master.scp")

while True:
    call = input("Callsign: ").upper()
    print(ctyData(call))
    print(scpData(call))
